<?php
return [
	"cancel" => "Hủy bỏ",
	"save"	=> "Lưu lại",
	"back"	=> "Trở vế",
	"edit"	=> "Chỉnh sửa",
	"delete"	=> "Xóa",
	"trash"	=> "Thùng rác",
	"spam"	=> "Spam",
	"placeholder_search" => "Nhập từ khóa",
	"btn_search" => "Tìm kiếm..",
	"create"	=> "Thêm mới",
	"movetrash"	=>	"Thùng rác",
];