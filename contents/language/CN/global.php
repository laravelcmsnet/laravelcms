
<?php
return[
"home" => "家",
"youarehere" => "您现在的位置",
"cancel" => "取消",
"save" => "留",
"back" => "回报",
"edit" => "编辑",
"delete" => "抹去",
"trash" => "箱子",
"spam" => "垃圾邮件",
"placeholder_search" => "输入关键字",
"btn_search" => "搜索..",
"create" => "添加新",
"movetrash" => "箱子",
"block" => "锁",
"account" => "帐户",
"add" => "添加",
];
?>