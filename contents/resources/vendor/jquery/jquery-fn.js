
jQuery.fn.extend({
    live: function (event, callback) {
       if (this.selector) {
            jQuery(document).on(event, this.selector, callback);
        }
    }
});

$.fn.scrollView = function () {
  return this.each(function () {
    $('html, body').animate({
      scrollTop: $(this).offset().top
    }, 1000);
  });
}
        
$.fn.centerScreen = function(){
    var height = $(this).outerHeight();
    var screenHeight = $(document).height();
    var fixTop = ((screenHeight - height) / 2);
    $(this).css({"margin":"auto", "margin-top" : fixTop});
};



$.fn.shoppings = function(id){
    var singer = $(this).attr("singer");
    $.ajax({
            url : '/invoices/order/create/'+id,
            type : "GET",
            //dateType:"html",
            //contentType: 'multipart/form-data; charset=UTF-8;',
            contentType : false,
            encoding: '', 
            processData: false,
            beforeSend: function () {
                
            },
            success : function (result){

                if(singer == "true"){
                    window.location.href="/invoices/order";
                }

            },
            complete: function () {
               

            },
            error: function () {
              
            }
        });
};


$.fn.remembers = function(){
   
};


$.fn.animateCss = function (animationName) {
        var fx = 'bounce flash pulse rubberBand shake swing tada wobble jello bounceIn bounceInDown bounceInLeft bounceInRight bounceInUp bounceOut bounceOutDown bounceOutLeft bounceOutRight bounceOutUp fadeIn fadeInDown fadeInDownBig fadeInLeft fadeInLeftBig fadeInRight fadeInRightBig fadeInUp fadeInUpBig fadeOut fadeOutDown fadeOutDownBig fadeOutLeft fadeOutLeftBig fadeOutRight fadeOutRightBig fadeOutUp fadeOutUpBig flip flipInX flipInY flipOutX flipOutY lightSpeedIn lightSpeedOut rotateIn rotateInDownLeft rotateInDownRight rotateInUpLeft rotateInUpRight rotateOut rotateOutDownLeft rotateOutDownRight rotateOutUpLeft rotateOutUpRight slideInUp slideInDown slideInLeft slideInRight slideOutUp slideOutDown slideOutLeft slideOutRight zoomIn zoomInDown zoomInLeft zoomInRight zoomInUp zoomOut zoomOutDown zoomOutLeft zoomOutRight zoomOutUp hinge rollIn rollOut';
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).removeClass(fx);
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
};


$.fn.hoverAnimate = function(animationName){
    var $this = $(this);
    $this.on("mouseenter",function(){
        $(this).animateCss(animationName);
    });
    /*
    .mouseleave(function(e){
        
        
    });
    */
};

$.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined;
};

$.fn.outHTML = function(value){
	// If there is no element in the jQuery object
    if(!this.length)
        return null;
    // Returns the value
    else if(value === undefined){
        
        var element = (this.length) ? this[0] : this,
            result;

        // Return browser outerHTML (Most newer browsers support it)
        if(element.outerHTML)
            result = element.outerHTML;
        // Return it using the jQuery solution
        else
            result = $(document.createElement("div")).append($(element).clone()).html();
        
        // Trim the result
        if(typeof result === "string")
            result = $.trim(result);
        
        return result;
        
    }
    // Deal with function
    else if( $.isFunction(value) ){
        
        this.each(function(i){
            var $this = $( this );
            $this.outerHTML( value.call(this, i, $this.outerHTML()) );
        });
        
    }
    // Replaces the content
    else {
        
        var $this = $(this),
            replacingElements = [],
            $value = $(value),
            $cloneValue;
        
        for(var x = 0; x < $this.length; x++){
            
            // Clone the value for each element being replaced
            $cloneValue = $value.clone(true);
            
            // Use jQuery to replace the content
            $this.eq(x).replaceWith($cloneValue);
            
            // Add the replacing content to the collection
            for(var i = 0; i < $cloneValue.length; i++)
                replacingElements.push($cloneValue[i]);
        
        }
        
        // Return the replacing content if any
        return (replacingElements.length) ? $(replacingElements) : null;

    }
};

(function ( $,window, document ) {
 
    $.fn.xcode = function(options) {
        var textarea = $(this);
        var mode = textarea.data('edit_code');
        var fullheight = textarea.data("fullheight");
        var theme = textarea.data("theme");
        theme = (theme ? theme : "dreamweaver");
        
        var setHeight = false;

        if(fullheight == "window"){
            setHeight = $(window).height() - ($(this).offset().top + 10);
            
        }
        var editDiv = $('<div>', {
          position: 'absolute',
          width: textarea.width(),
          height: (setHeight ? setHeight : textarea.height()),
          'class': textarea.attr('class')
        }).insertBefore(textarea);
        textarea.css({'visibility':'hidden','display':'none'});
        var editor = ace.edit(editDiv[0]);
        editor.renderer.setShowGutter(textarea.data('gutter'));
        editor.getSession().setValue(textarea.val());
        editor.getSession().setMode("ace/mode/" + mode);
        editor.setTheme("ace/theme/" + theme);

        // copy back to textarea on form submit...
        textarea.closest('form').submit(function() {

          textarea.val(editor.getSession().getValue());
          
        });
    };
 
}( jQuery,window, document ));
(function ( $,window, document ) {
 
    $.fn.loadMore = function(options) {
       
         var settings = $.extend({   
            navSelector: '', //Navigation selector/Pagination selector e.g. "nav.navigation"
            contentSelector: '',//Main content element selector e.g. #main
            nextSelector: '',//Navigation next element selector e.g. nav.navigation a.next
            itemSelector: '',//Item container element in loop e.g. article.post
            paginationType: 'infinite',//Pagination type Infinite scroll or Load more button.Default  "infinite"
            loadingImage:'',//Loading image url.Default null
            loadingButtonLabel: 'Load More',//Load more button text.Default "Load More"
            loadingButtonClass: '',//Load more button additional class.Default null
            loadingFinishedText: 'No More Posts Available',//Text to show when loading is finished.Default "No More Posts Available"
        }, options );
        
        var loading=false;
        var loaded=false;
        var url=false;
        var infiniteHtml='';
        var moreHtml='';
        settings.itemSelector = ( settings.itemSelector ?  settings.itemSelector : $(this));
        
        var _init=function()
        {
            //check if all the elements given are correct
            if($(settings.navSelector).length && $(settings.contentSelector).length && $(settings.nextSelector).length && $(settings.itemSelector).length)
            {               
                url=$(settings.nextSelector).attr( 'href' );
                _setup_element();               
            }
            else
            {
                return false;
            }   
        }
        
        var _setup_element=function(){
            
            $(settings.navSelector).hide();
            
            infiniteHtml=((settings.loadingImage!='')?'<img src="'+settings.loadingImage+'" />':'');
            moreHtml='<input type="button" class="'+settings.loadingButtonClass+'" value="'+settings.loadingButtonLabel+'" />';
            
            switch(settings.paginationType) {
            case 'infinite':
                $(settings.navSelector).before('<div class="pix-wrapper pix-loader">'+infiniteHtml+'</div>');
                break;
            case 'more':
                $(settings.navSelector).before('<div class="pix-wrapper pix-load-more">'+moreHtml+'</div>');
                break;
            default:
                $(settings.navSelector).before('<div class="pix-wrapper">Loading.....</div>');
            }           
        
        }
        
        var _load=function(){
            
            //check if url exixts
            if(!url)
            return false;
            
            loading = true;
            
             var lastElem   = $( settings.contentSelector ).find( settings.itemSelector ).last();
             
             //custom trigger when start loading
             $(document).trigger('pix_infinite_load_start');            
            
            // ajax call
            $.ajax({
                // params
                url         : url,
                dataType    : 'html',
                success     : function (response) {

                loading = false;
                
                 if(settings.paginationType=='infinite')
                 $('.pix-loader').hide();
                
                
                 var obj  = $(response),
                        elem = obj.find( settings.itemSelector ),
                        next = obj.find( settings.nextSelector );
                
                lastElem.after( elem ); 
                 
                 //custom trigger when successfully loaded
                 $(document).trigger('pix_infinite_load_success');
                
                    if( next.length ) {
                        url = next.attr( 'href' );
                    }
                    else {
                       loaded=true;
                       
                        //custom trigger when successfully loaded all pages
                        $(document).trigger('pix_infinite_load_complete');
                     
                    }
                
                }
            });
            
        };
        
        $(window).on('scroll',function(){
                                       
            if(! loading && ! loaded && settings.paginationType=='infinite' && $(window).scrollTop() >= $(settings.itemSelector).last().offset().top + $(settings.itemSelector).last().outerHeight() - window.innerHeight)  
            {
               _load();
            }   
        });
        
        $(document).on('click','.pix-load-more',function(){
            if(! loading && ! loaded && settings.paginationType=='more'){
                
                $('.pix-wrapper').html(infiniteHtml);
                
                _load();                                 
            }
        });
        
        $(document).on('pix_infinite_load_start',function(){
            if(settings.paginationType=='infinite')
            $('.pix-wrapper').show();                                                  
        });
        
        $(document).on('pix_infinite_load_success',function(){
            if(settings.paginationType=='infinite')
            $('.pix-wrapper').hide();
            else if(settings.paginationType=='more')
            $('.pix-wrapper').html(moreHtml);
        });
        
        $(document).on('pix_infinite_load_complete',function(){ 
             $('.pix-wrapper').html(settings.loadingFinishedText).show();
        });
        
        //Initialization
        _init();
        
        
    };
 
}( jQuery,window, document ));
