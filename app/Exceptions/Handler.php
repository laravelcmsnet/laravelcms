<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

         if($e instanceof NotFoundHttpException){
            header('HTTP/1.0 404 Not Found');
            return views("errors.404",[],404);
        }else if($e instanceof ModelNotFoundException){
            header('HTTP/1.0 404 Not Found');
            return views("errors.404",[],404);
        }else{
            if(config("app.debug")){
                echo $e->getMessage(), '<br>';
                echo nl2br(htmlentities($e->getTraceAsString()));
            }else{
               header('HTTP/1.0 404 Not Found');
               echo views("errors.404");
            }
        }
        exit();
        die();

        return parent::render($request, $e);
    }
}
