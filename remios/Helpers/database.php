<?php
if(!function_exists("pages")){
    function pages($obj, $append=[], $loadmore=false, $loadmoreItems="div", $button=""){
        if(method_exists($obj, "render")){
            
            if($append){
                $obj->appends($append);
            }
            //$obj->lastPage();
            if($loadmore){
               
                register("javascript",[resources_url("jquery/jquery.infinitescroll.js")]);
                if($button){
                    echo '<a class="btn btn-default btn-loadmore">'.$button.'</a>';
                    echo '<div id="loadmorePages" style="display:none;">'.$obj->render().'</div>';
                    echo '<script>
                    jQuery(document).ready(function(){
                        $(".btn-loadmore").on("click", function(){
                            $("'.$loadmore.'").infinitescroll("resume");
                            // check if new elements should be loaded
                            $("'.$loadmore.'").infinitescroll("scroll");
                        });
                        var loading_options = {
                            finishedMsg: "<div class=\'end-msg\'>Congratulations! You\'ve reached the end of the internet</div>",
                            msgText: "<div class=\'center\'>Loading news items...</div>",
                            img: "/assets/img/ajax-loader.gif"
                        };

                        $("'.$loadmore.'").infinitescroll({
                          loading : loading_options,
                          navSelector : "#loadmorePages .pagination",
                          nextSelector : "#loadmorePages .pagination li.active + li a",
                          itemSelector : "'.$loadmore.' > '.$loadmoreItems.'",

                        },
                        function(){
                            $("'.$loadmore.'").infinitescroll("pause")
                        });
                        
                        $(window).scroll(function () {
                            if ($(window).scrollTop() >= $(window).height() - 50) {
                                var bLazy = new Blazy({ 
                                    breakpoints: [{
                                                  width: 420 // max-width
                                            , src: \'data-src-small\'
                                             }
                                               , {
                                                  width: 768 // max-width
                                                , src: \'data-src-medium\'
                                        }],

                                });
                            }
                        });
                    });
                    
                    </script>';
                }else{
                    
                echo '<div id="loadmorePages">'.$obj->render().'</div>';
                echo '<script>
                   jQuery(document).ready(function(){

                    var loading_options = {
                        finishedMsg: "<div class=\'end-msg\'>Congratulations! You\'ve reached the end of the internet</div>",
                        msgText: "<div class=\'center\'>Loading news items...</div>",
                        img: "/assets/img/ajax-loader.gif"
                    };

                    $("'.$loadmore.'").infinitescroll({
                      loading : loading_options,
                      navSelector : "#loadmorePages .pagination",
                      nextSelector : "#loadmorePages .pagination li.active + li a",
                      itemSelector : "'.$loadmore.' > '.$loadmoreItems.'",

                    });
                    $(window).scroll(function () {
                        if ($(window).scrollTop() >= $(window).height() - 50) {
                            var bLazy = new Blazy({ 
                                breakpoints: [{
                                              width: 420 // max-width
                                        , src: \'data-src-small\'
                                         }
                                           , {
                                              width: 768 // max-width
                                            , src: \'data-src-medium\'
                                    }],
                            
                            });
                        }
                    });
                });

                </script>';
                }
            }else{
                echo $obj->render();
            }
        }
    }
}


function hasTable($table=""){
    if(!$table) return false;
    if(\Schema::hasTable($table)){
        return true;
    }else{
        return false;
    }
}


function db_backups($path="", $table=true, $data=true){
	$db = with(new Remios\Utils\Database\Backups)->saveOutput($path);
}

function db_restore($file=""){
	sql_import_file($file, false);
}


if(!function_exists("sql_import_file")){
	function sql_import_file($files = null, $backup=true,$connect=false){
		$db = with(new Remios\Utils\Database\Backups);
		if($backup){
           
            $db->save();
        }
		if (files()->exists($files)) {
            
            $sql_query = fread(fopen($files, 'r'), filesize($files)) or die('problem ' . __FILE__ . __LINE__);
            $sql_query = str_ireplace('{stores_id}', config("site.stores_id"), $sql_query);
            $sql_query = $db->remove_sql_remarks($sql_query);
            $sql_query = $db->remove_comments_from_sql_string($sql_query);
            $sql_query = $db->split_sql_file($sql_query, ';');
            
            foreach ($sql_query as $sql) {
                $sql = trim($sql);
                if($connect){
                    \DB::connection($connect)->statement($sql);
                }else{
                    \DB::statement($sql);
                }

            }
        }
	}
}


if(!function_exists("sql_truncate")){
	function sql_truncate(){
		with(new Remios\Utils\Database\Backups)->truncate();
	}
}

if(!function_exists("sql_query")){
	function sql_query($sql_query = null, $connect=false){
		if(!$sql_query) return false;
		$db = with(new Remios\Utils\Database\Backups);
		
        $sql_query = str_ireplace('{stores_id}', config("site.stores_id"), $sql_query);
        $sql_query = $db->remove_sql_remarks($sql_query);
        $sql_query = $db->remove_comments_from_sql_string($sql_query);
        $sql_query = $db->split_sql_file($sql_query, ';');
       
        foreach ($sql_query as $sql) {
            $sql = trim($sql);
            if($connect){
                \DB::connection($connect)->statement($sql);
            }else{
                \DB::statement($sql);
            }

        }
        
	}
}
