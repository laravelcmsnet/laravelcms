<?php
namespace Modules\Catalog\Models;
use Remios\Utils\Database\DBQuery;
class Categories extends DBQuery{
	protected $table = 'categories';
	public $timestamps = false;
    public $post_maps_id = [];
    public $post_maps_url = [];
	
	public function links(){
        if(config("register.router.".$this->type)){
            return base_url(config("register.router.".$this->type)."/catalog").($this->seo_urls ? "/".$this->seo_urls : '?pid='.$this->id);
        }else if(config("register.pages.urls") == $this->type){
            return base_url("pages/".config("register.pages.urls")."/catalog").($this->seo_urls ? "/".$this->seo_urls : '?pid='.$this->id);
        }else{
            return base_url("catalog").($this->seo_urls ? "/".$this->seo_urls : '?pid='.$this->id);
        }
        
    }

    public function pages_maps($parent=""){
        $this->mapsPages($parent);
        return implode($this->post_maps_id, "|");
    }


    function mapsPages($parent=false){
        
        $parent = ($parent ? $parent : $this->parent_id);
        if(intval($parent) == 0) return false;
        $opt = [];
        if(intval($parent) > 0){
            $data = $this->find($parent);
            if(isset($data->id)){
                if($data->parent_id > 0){
                    $this->mapsPages($data->parent_id);
                }
                $this->post_maps_url[] = str_slug($data->title);
                $this->post_maps_id[] = $data->id;
            }
        }
        
    }

    public function getMaps($thisid=false, $replace_base=true){
        if(!$this->pages_maps) return [];
        $cache = cache()->get("catalog");
        
        if($cache){
            $ex = explode("|", $this->pages_maps.($thisid ? "|".$this->id : ""));
            $maps = [];
            foreach ($ex as $key => $value) {
                if(isset($cache[$value])){
                    $data = $this->find($value);
                     $url = ($replace_base ? str_replace(base_url(), '', $cache[$value]["url"]) : $cache[$value]["url"]);
                     $maps[$url] = $cache[$value]["name"]; 
                   
                }
                 
            }
        }else{
            $ex = explode("|", $this->pages_maps.($thisid ? "|".$this->id : ""));
            $maps = [];
            foreach ($ex as $key => $value) {
                 $data = $this->find($value);
                 $url = ($replace_base ? str_replace(base_url(), '', $data->links()) : $this->links());
                 $maps[$url] = $data->title; 
            }
        }
        return $maps;
    }

     public function seo_urls($url=""){
        $data = db("Catalog::Categories")->language()->stores()->where("seo_urls",$url)->where("id","!=",$this->id)->count();
        if($data > 0){
            return str_replace('.html', '-'.($data + 1).'.html', $url);
        }else{
            return $url;
        }
    }


	public function users()
    {
        //return $this->hasOne('Modules\Account\Models\Users','id','users_id');
        return \Modules\Account\Models\Users::find($this->users_id);
    }



    public function counts(){
        return db("Posts::Posts",$this->type)->where("categories_id", $this->id)->where("type", $this->type)->count();
    }
    

    public function created_at($format=null){
        
        if(!$format) $format = config("site.time_short_format","d-m-Y");
        $mytime =  \Carbon\Carbon::parse($this->created_at);
        $mytime->setTimezone(config("site.timezone"));
        return $mytime->format($format);
    }

    public function updated_at($format=null){
        
        if(!$format) $format = config("site.time_short_format","d-m-Y");
        $mytime =  \Carbon\Carbon::parse($this->created_at);
        $mytime->setTimezone(config("site.timezone"));
        return $mytime->format($format);
    }
}