<?php
return [
	"leftmenu" => [
                    
                    "name" => "Nội dung web",
                    "icons" =>  "glyphicon glyphicon-tasks",
                    "contents" => [
                            "stores/settings" => "Điều chỉnh Stores",
                            "stores/content/templates" => "Nội dung mẫu",
                            "stores/email/manager" => "Nội dung email",            
                        ],
                    "sort"  => "n"
                ]
];
?>