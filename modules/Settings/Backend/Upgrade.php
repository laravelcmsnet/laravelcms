<?php
namespace Modules\Settings\Backend;
use Remios\Apps\Admin;
class Upgrade extends Admin
{
    public $_server = "http://laravelcms.net/upgrade.php";
    public $_server_db = "http://laravelcms.net/database.json";
	public function __construct()
    {
        
        
        if(user()->is_admin != 1 || user()->id != 1){  
            
            return redirect(admin_url("account/profiles",false))->with("error", "You Are Not Access")->send();
            
        }
        parent::__construct();
    }

	function getIndex($a=""){
        $modules = [];
        $systems = [];
        $list = glob(base_path("modules/*"));
        foreach ($list as $key => $value) {
            if(!file_exists($value."/system.txt")){
                if(is_dir($value)) $modules[] = $value;
            }else{
                if(is_dir($value)) $systems[] = $value;
            }
        }

    	return views("upgrade-systems",["modules" => $modules, "systems" => $systems]);
    }


    function getProcess($module="", $filter=""){
        $path_save = base_path("contents/upgrade/{$module}.zip");
        $server_path = $this->_server."?module=".$module;
        files()->put($path_save, file_get_contents($server_path));

        return redirect()->back()->with("success", "Updage success")->send();
    }


    function getDatabase(){
       $data = json_decode(file_get_contents($this->_server_db));
       $tables = [];
        $result = \DB::select('SHOW TABLES');
        if (!empty($result)) {
            foreach ($result as $item) {
                $item = (array)$item;
                if (count($item) > 0) {
                    $item_vals = (array_values($item));
                    $tables[] = $item_vals[0];
                }
            }
        }
        $sql = [];
        $tables_filed = [];
        foreach ($tables as $key => $value) {
            $columns = \DB::select('SHOW COLUMNS FROM '.$value);
            $column = [];
            $after = "id";
            foreach ($columns as $key_c => $value_c) {
                if($value_c->Field != "id"){
                   
                    $column[$value_c->Field] = "'".$value_c->Type."'";
                     

                }
                $after = $value_c->Field;
            }
            $tables_filed[$value] = $column;
        }

        
        foreach ($data as $key => $value) {
            if(isset($tables_filed[$key])){
                foreach ($value as $key_c => $value_c) {
                    if(isset($tables_filed[$key][$key_c]) && $tables_filed[$key][$key_c] != $value_c->validate){
                        $sql[] = $value_c->update;
                    }else if(!isset($tables_filed[$key][$key_c])){
                        $sql[] = $value_c->add;
                    }
                    
                }
            }
        }
        sql_query(implode($sql, "\n"));
        return redirect()->back()->with("success", implode($sql, "\n"));
        
    }
}