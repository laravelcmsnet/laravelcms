<?php
function pages_shortcode($atts = [], $content=""){
			extract( shortcode_atts( array(
		      "type"    => "blogs",
		      "layout"	=> "",
		      "xclass"  => "col-sm-12 col-xs-12",
		      "pclass"	=>	"row",
		      "limit"	=>	"20",
		      "order"	=>	"orders,asc",
		      "search"	=>	"",
		      "tags"	=>	false,
		      "parent"	=> 0,
		      "icons"	=>	"",
		      "submenu"	=> false,
		      
		    ), $atts ) );
			
			
			list($order_k, $order_v) = explode(',', $order);
		    $data = db("Pages::Pages")->language()->stores()->where("parent_id",$parent)->orderBy(trim($order_k),trim($order_v))->get();
		    $icons = ($icons ? "<i class=\"icons fa ".$icons."\"></i> " : "");
		    $html = [];
		    $html[] = '<div class="vertical-group vertical"><ul class="row">';
		    foreach ($data as $key => $value) {
		    	$countChild = 0;
		    	if($submenu){
		    	 $countChild = db("Pages::Pages")->language()->stores()->where("parent_id",$value->id)->count();
		    	 
		    	}
		    	$arrow = ($countChild > 0  ? '<span class="pull-right glyphicon glyphicon-menu-right"></span>' : "");
		    	
		    	$html[] = '<li class="'.$xclass.(config("register.pages.active") == $value->id ? " active" : "").'"><a href="'.$value->links(true).'" title="'.$value->title.'">'.$icons.$value->title.$arrow.'</a>';
		    	
		    	 if($countChild > 0){
		    	 	 $html[] = do_shortcode('[pages parent="'.$value->id.'"][/pages]');
		    	 }
		    	 $html[] = '</li>';
		    }
		     $html[] = '</ul></div>';
		     return implode($html,"\n");

}

add_shortcode("pages","pages_shortcode");




function pages_content_shortcode($atts = [], $content=""){
			extract( shortcode_atts( array(
		      "urls"    => false,
		      "class"	=> 'btn btn-info',
		      "icons"	=> false,
		      "slipt"	=> false,
		      "limit"	=> false,
		    ), $atts ) );

		    $data = db("Pages::Pages")->language()->stores()->where("seo_urls",$urls)->first();
		   
		    $html = [];
		    if($slipt){
		    	$extract = explode('<!-- pagebreak -->', $data->content);
		    	$des = $extract[0];
		    }else{
		    	$des = $data->description;
		    }

		    $icons = ($icons ? "<i class=\"icons fa ".$icons."\"></i> " : "");

		    $html[] = '<p>'.$des.'</p><a class="'.$class.' readmore" href="'.$data->links().'">'.$icons.lang("globals.more").'</a>';
		   
		     return implode($html,"\n");

}

add_shortcode("pages_contents","pages_content_shortcode");


function getPagesRouter($select=""){
	$thml = [];
	
	foreach (config("register.router") as $key => $value) {
		$thml[] =  '<option value="'.$key.'" '.($key == $select ? "selected" : "").'>'.$value.'</option>';
	}

	return implode($thml, "\n");


}