<?php
namespace Modules\Pages\Widgets;
class Pages{


	public function register(){
		return [
				"name" => "Pages Menu",
				"icons"	=>	"",
				"auth"	=> "",
				"source"	=> ""
			];
	}


	public function main($data = []){
		
		$parent = (@$data["parent"] == "auto" ? config("register.pages.parent_id",0) : @$data["parent"]);
		$limit = (!@$data["limit"] || @$data["limit"] == "" ? 5 : $data["limit"]);

		$dataReturn = do_shortcode('[pages  parent="'.$parent.'" '.(@$data["icons"] ? 'icons="fa '.@$data["icons"].'"' : "").' '.( @$data["autosubmenu"] == 1 ? 'submenu="true"' : "").' xclass="col-xs-12 col-sm-'.data(@$data["size"],12).'" limit="'.@$data["limit"].'"][/pages]');
		if(strlen($dataReturn) < 100){
			return false;
		}
		return $dataReturn;
	}


	public function admin($data = []){

		echo '
		<ul class="row">
			<li class="col-xs-12">
				Type
				<div class="input-group">
						      <span class="input-group-btn">
						      	<input type="hidden" class="iconsSet" name="content[icons]" value="'.@$data["icons"].'">
						        <button class="btn btn-default" role="iconpicker"  data-icon="'.@$data["icons"].'" type="button"><i class="glyphicon glyphicon-repeat"></i></button>
						      </span>
				<select name="content[parent]" class="form-control selectpicker">
					<option value="0">Root Parent</option>
					<option value="auto" '.(@$data["parent"] == "auto" ? "selected" : "").'>Auto Detect</option>
					';
				pages_options(@$data["parent"],["parent_id" => 0],true);
		echo '		</select>
				</div>
			</li>
			<li class="col-xs-6">
				Sort By
				<select name="content[sort_by]" class="form-control selectpicker">
					<option value="id" '.(@$data["sort_by"] == "id" ? "selected" : "").'>New</option>
					<option value="created_at" '.(@$data["sort_by"] == "created_at" ? "selected" : "").'>Create Date</option>
					<option value="updated_at" '.(@$data["sort_by"] == "id" ? "selected" : "").'>Update Date</option>
					<option value="title" '.(@$data["sort_by"] == "updated_at" ? "selected" : "").'>Name</option>
					<option value="ratings" '.(@$data["sort_by"] == "ratings" ? "selected" : "").'>Ratings</option>
				</select>
			</li>
			<li class="col-xs-6">
				Sort Order
				<select name="content[order_by]" class="form-control selectpicker">
					<option value="desc" '.(@$data["order_by"] == "desc" ? "selected" : "").'>DESC</option>
					<option value="asc" '.(@$data["order_by"] == "asc" ? "selected" : "").'>ASC</option>
				</select>
			</li>


			<li class="col-xs-6">
				Limit
				<input class="form-control" name="content[limit]" value="'.data(@$data["limit"],10).'"/>
			</li>
			<li class="col-xs-6">
				Layout
				<select name="content[size]" class="form-control selectpicker">
					<option value="12" '.(@$data["size"] == "12" ? "selected" : "").'>1</option>
					<option value="6" '.(@$data["size"] == "6" ? "selected" : "").'>1/2</option>
					<option value="4" '.(@$data["size"] == "4" ? "selected" : "").'>1/3</option>
					
				</select>
			</li>

			<li class="col-xs-12">
				Tag\'s
				<input class="form-control" name="content[tags]" value="'.@$data["tags"].'" />
					
			</li>

			<li class="col-xs-12">
				<label class="checkbox-inline">
					<input type="checkbox" name="content[autosubmenu]" value="1" '.(@$data["autosubmenu"] == 1 ? "checked" : "").' /> Auto Submenu
				</label>
			</li>


		</ul>
		';
	}
}