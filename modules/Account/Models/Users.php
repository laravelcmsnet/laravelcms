<?php
namespace Modules\Account\Models;
use Remios\Utils\Database\DBQuery;
class Users extends DBQuery{
	protected $table = 'users';
	public $timestamps = false;
	
	
    public function createByEmail($email="", $password=""){
		$userid = "";
		$input = [
            'email' => $email,
            'password' => $password,
        ];
        $rules = [
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|max:255|min:8',
        ];
        
        $validator = app("validator")->make($input, $rules);
        if (!$validator->fails()) {
			$userid = $this->insertGetid([
                    "email" => strtolower($email),
                    "password" => \Hash::make(strtolower($password)),
                    "is_active" => 1,
                    "language"  =>  getLanguage(),
                ]);
		}
		return $userid;
	}

    public function getIdFormEmail($email=""){
        $data = $this->where("email",$email)->first();
        if(isset($data->id)) return $data->id;
        return 0;
    }
}